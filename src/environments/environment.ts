// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBQtmKFGXRdIiNgupNuzKwRMSdpzFYwNP0",
    authDomain: "trainingmanagementsystem-4f7b5.firebaseapp.com",
    projectId: "trainingmanagementsystem-4f7b5",
    storageBucket: "trainingmanagementsystem-4f7b5.appspot.com",
    messagingSenderId: "17536915088",
    appId: "1:17536915088:web:5915633e0221f669b5aa15",
    measurementId: "G-BX6VX6DJ28"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
// Import the functions you need from the SDKs you need
