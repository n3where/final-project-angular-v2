import {initializeApp} from "@angular/fire/app";
import {getDocs, collection, getFirestore} from "@angular/fire/firestore";
import {getAuth, onAuthStateChanged} from "@angular/fire/auth";

const firebaseApp = initializeApp({
  apiKey: "AIzaSyBQtmKFGXRdIiNgupNuzKwRMSdpzFYwNP0",
  authDomain: "trainingmanagementsystem-4f7b5.firebaseapp.com",
  projectId: "trainingmanagementsystem-4f7b5",
  storageBucket: "trainingmanagementsystem-4f7b5.appspot.com",
  messagingSenderId: "17536915088",
  appId: "1:17536915088:web:5915633e0221f669b5aa15",
  measurementId: "G-BX6VX6DJ28"
});

const auth = getAuth(firebaseApp);
const db = getFirestore(firebaseApp);
const todosCol = collection(db,'todos');
db.collection('todos').getDocs();

const snapshot = await getDocs(todosCol);

auth.onAuthStateChanged(user =>{

});
onAuthStateChanged(auth, user =>{
  if (user != null) {
    console.log("Logged in!");
  } else {
    console.log('No user');
  }
});
