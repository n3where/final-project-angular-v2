import { Component, OnInit } from '@angular/core';
import {UserReturnDto} from "../model/user/userReturnDto";
import {UserService} from "../service/user.service";

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {
  user: UserReturnDto = this.userService.getUser();
  name: string

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.name = 'User'
  }

  isNotAdmin():boolean{
    if(this.userService.getUser().type === 'ADMIN' || this.userService.getUser().type === 'LEADER'){
      return false
    }
    return true
  }

}
