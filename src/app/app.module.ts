import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from "@angular/router";
import {AppComponent} from './app.component';
import {CoursesComponent} from './courses/courses.component';
import {AdminCoursesComponent} from './admin/admin-courses/admin-courses.component';
import {AdminUsersComponent} from './admin/admin-users/admin-users.component';
import {ShoppingCartComponent} from './shopping-cart/shopping-cart.component';
import {ApplicationSuccessComponent} from './application-success/application-success.component';
import {LoginComponent} from './login/login.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {MycoursesComponent} from './mycourses/mycourses.component';
import {BsNavbarComponent} from './bs-navbar/bs-navbar.component';
import {CourseFormComponent} from './admin/course-form/course-form.component';
import {HttpClientModule} from "@angular/common/http";
import {ModuleListComponent} from './module-list/module-list.component';
import {UserRegistrationComponent} from './user-registration/user-registration.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ModuleFormComponent} from './module-form/module-form.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatCheckboxModule} from '@angular/material/checkbox';
import {HomePageComponent} from './home-page/home-page.component';
import {CourseListComponent} from './course-list/course-list.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatTableModule} from '@angular/material/table';
import { LogoutComponent } from './logout/logout.component';
import { ReturnAplicationComponent } from './return-aplication/return-aplication.component';


@NgModule({
  declarations: [
    AppComponent,
    BsNavbarComponent,
    CoursesComponent,
    AdminCoursesComponent,
    AdminUsersComponent,
    ShoppingCartComponent,
    ApplicationSuccessComponent,
    LoginComponent,
    MycoursesComponent,
    BsNavbarComponent,
    CourseFormComponent,
    ModuleListComponent,
    UserRegistrationComponent,
    ModuleFormComponent,
    HomePageComponent,
    CourseListComponent,
    LogoutComponent,
    ReturnAplicationComponent,
  ],

  imports: [
    BrowserModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    NgbModule,
    MatCheckboxModule,
    RouterModule.forRoot([
      {path: '', component: HomePageComponent},
      //courses
      {path: 'app-courses', component: CoursesComponent},
      {path: 'shopping-cart', component: ShoppingCartComponent},
      //login
      {path: 'login', component: LoginComponent},
      //logout
      {path:'app-logout',component:LogoutComponent},
      {path:'app-return-aplication',component:ReturnAplicationComponent},
      {path: 'admin/courses', component: AdminCoursesComponent},
      {path: 'app-course-form', component: CourseFormComponent},
      {path: 'admin/users', component: AdminUsersComponent},
      {path: 'my/courses', component: MycoursesComponent},
      // user register
      {path: 'registration', component: UserRegistrationComponent},
      //formularu de creare module
      {path: 'module-form', component: ModuleFormComponent},
      //lista de module
      {path: 'modules', component: ModuleListComponent}
    ]),
    FormsModule,
    MatButtonModule,
    MatTabsModule,
    MatDatepickerModule
  ],
  providers: [],
  bootstrap: [
    AppComponent],
  exports: [
    RouterModule
  ]
})
export class AppModule {
}
