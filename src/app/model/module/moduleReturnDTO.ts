export class ModuleReturnDTO {
  id: string;
  name: string;
  description: string;
  isChecked: boolean;


  constructor(id: string, name: string, description: string, isChecked: boolean) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.isChecked = isChecked;
  }
}
