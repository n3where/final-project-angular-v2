// @ts-ignore
import {ModuleReturnDto} from "../module/moduleDto";

export class CourseReturnDto {
  id: number
  name:String;
  listOfModule: ModuleReturnDto[];
  description: string;
  isChecked: boolean


  constructor(id: number, name: String, listOfModule: [], description: string, isChecked: boolean) {
    this.id = id;
    this.name = name;
    this.listOfModule = listOfModule;
    this.description = description;
    this.isChecked = isChecked;
  }
}
