export class CourseCreateDto {

  name: string;
  moduleId: number[];
  description: string;


  constructor(name: string, moduleId: number[], description: string) {
    this.name = name;
    this.moduleId = moduleId;
    this.description = description;
  }
}
