export class CourseInstanceCreateDto{

  courseId: number
  courseStartDate: string
  courseEndDate: string
  name: string


  constructor(courseId: number, courseStartDate: string, courseEndDate: string, name: string) {
    this.courseId = courseId;
    this.courseStartDate = courseStartDate;
    this.courseEndDate = courseEndDate;
    this.name = name;
  }
}
