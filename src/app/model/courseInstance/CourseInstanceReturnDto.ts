import {UserReturnDto} from "../user/userReturnDto";

export class CourseInstanceReturnDto{
  courseName: string
  courseDescription: string
  startDate: string
  endDate: string
  userList: UserReturnDto[]


  constructor(courseName: string, courseDescription: string, startDate: string, endDate: string, userList: UserReturnDto[]) {
    this.courseName = courseName;
    this.courseDescription = courseDescription;
    this.startDate = startDate;
    this.endDate = endDate;
    this.userList = userList;
  }
}
