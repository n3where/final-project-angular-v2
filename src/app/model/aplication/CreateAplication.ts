export class CreateAplication{
  userId: number
  dateOfApplication: string
  courseId: number

  constructor(userId: number, dateOfApplication: string, courseId: number) {
    this.userId = userId;
    this.dateOfApplication = dateOfApplication;
    this.courseId = courseId;
  }
}
