export class ReturnAplication{
  userName:string
  dateOfApplication:string
  courseName:string
  accepted:string


  constructor(userName: string, dateOfApplication: string, courseName: string, accepted: string) {
    this.userName = userName;
    this.dateOfApplication = dateOfApplication;
    this.courseName = courseName;
    this.accepted = accepted;
  }
}
