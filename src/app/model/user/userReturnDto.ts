export class UserReturnDto {
  id:number
  userName: string;
  type: string;
  firstName: string;
  lastName: string;


  constructor(id: number, userName: string, type: string, firstName: string, lastName: string) {
    this.id = id;
    this.userName = userName;
    this.type = type;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
