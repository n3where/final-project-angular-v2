import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ModuleService} from "../service/module.service";
import {Router} from "@angular/router";
import {ModuleCreateDto} from "../model/module/moduleCreateDto";

@Component({
  selector: 'module-form',
  templateUrl: './module-form.component.html',
  styleUrls: ['./module-form.component.css']
})
export class ModuleFormComponent implements OnInit {

  moduleForm = new FormGroup({
    name: new FormControl(''),
    description: new FormControl('')
  });

  constructor(
    private moduleService: ModuleService,
    private router: Router

  ) {
  }

  ngOnInit(): void {

  }

  onSubmit() {
    // const submitMessage ='Module\n'+
    //   'Module name: ' + this.moduleForm.get('name');
    // alert(submitMessage);


    const moduleDto: ModuleCreateDto={
      name: this.moduleForm.get('name')?.value,
      description: this.moduleForm.get('description')?.value
    };
    this.moduleService.addModule(moduleDto).subscribe(response=>{
      this.router.navigate(['module-form'])
    })
    this.moduleForm =new FormGroup({
      name: new FormControl(''),
      description: new FormControl('')
    })
  }
}

