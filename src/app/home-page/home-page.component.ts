import {Component, OnInit} from '@angular/core';
import {CourseService} from "../service/course.service";
import {CourseReturnDto} from "../model/course/courseReturnDto";
import {UserReturnDto} from "../model/user/userReturnDto";
import {CreateAplication} from "../model/aplication/CreateAplication";
import {AplicationServiceService} from "../service/aplication-service.service";
import {UserService} from "../service/user.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {LoginComponent} from "../login/login.component";
import {ReturnAplication} from "../model/aplication/ReturnAplication";
import {Route, Router} from "@angular/router";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  courseList: CourseReturnDto[] = []
  aplication!: FormGroup;
  userId:number ;
  auth:boolean
  user:UserReturnDto

  constructor( private formBuilder: FormBuilder,
               private courseService: CourseService,
               private aplicationService: AplicationServiceService,
               private userService:UserService,
               private route:Router) {
    this.userId = 0;
    this.auth=this.userService.isAuthenticated()
    this.user = this.userService.getUser()
  }

  ngOnInit(): void {
    this.getCourses()
    this.aplication= this.formBuilder.group({
      userId:new FormControl(),
      dateOfApplication: new FormControl(''),
      courseId: new FormControl()
    })
    this.userId = this.userService.getUser().id;
  }

  getCourses() {
    this.courseService.getAll().subscribe((course) => {
      this.courseList = course
    })
  }

  participate(courseId: number) {
    const app: CreateAplication = {
      //Rezolvat cu user id
      userId:this.userId,
      dateOfApplication: this.aplication.get('dateOfApplication')?.value,
      courseId: courseId
    }


    this.aplicationService.create(app).subscribe(response => {
      const submitMessage = 'Your aplication have been send'//todo o pagina de returnAplication unde iti spune daca esti acceptat sau nu
      alert(submitMessage);
      this.route.navigate(['app-return-aplication'])
    })
  }
}
