import { Component, OnInit } from '@angular/core';
import {ReturnAplication} from "../model/aplication/ReturnAplication";
import {AplicationServiceService} from "../service/aplication-service.service";
import {UserService} from "../service/user.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-return-aplication',
  templateUrl: './return-aplication.component.html',
  styleUrls: ['./return-aplication.component.css']
})
export class ReturnAplicationComponent implements OnInit {

  returnApp:ReturnAplication = this.aplicationService.returnAplication

  constructor(private aplicationService:AplicationServiceService,private user:UserService) { }

  ngOnInit(): void {
    this.aplicationService.returnApl(this.user.getUser().id)
  }


}
