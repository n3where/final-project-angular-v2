import {Component, OnInit} from '@angular/core';
import {ModuleService} from "../service/module.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup} from "@angular/forms";
import {UserService} from "../service/user.service";

@Component({
  selector: 'registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  userForm = new FormGroup({
    userName: new FormControl(''),
    password: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl('')
  })


  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    // const submitMessage = 'Account created\n' +
    //   'Username: ' + this.userForm.get('userName')?.value;
    //   alert(submitMessage);

    const userDto = {
      userName: this.userForm.get('userName')?.value,
      password: this.userForm.get('password')?.value,
      firstName: this.userForm.get('firstName')?.value,
      lastName: this.userForm.get('lastName')?.value
    };

    this.userService.createUser(userDto).subscribe(response =>{
      this.router.navigate(['login'])
    })
  }



}
