import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserService} from "./user.service";
import {CourseCreateDto} from "../model/course/courseCreateDto";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  private courseUrl = 'api/v1/course'
  private httpCLient;


  constructor(httpCLient:HttpClient, private userService: UserService) {
    this.httpCLient = httpCLient;
  }

  public addCourse(courseCreateDto: CourseCreateDto): Observable<any>{
    return this.httpCLient.post(this.courseUrl + "/post",courseCreateDto,this.userService.optionWithAuthorizationHeader)
  }

  public getAll():Observable<any>{
    return this.httpCLient.get(this.courseUrl + "/all",this.userService.optionWithAuthorizationHeader) as Observable<CourseCreateDto[]>
  }
}
