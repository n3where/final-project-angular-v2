import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserService} from "./user.service";
import {CourseInstanceCreateDto} from "../model/courseInstance/CourseInstanceCreateDto";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CourseInstanceService {

  private courseInstanceUrl = 'api/v1/courseInstance'
  private httpClient

  constructor(httpClient: HttpClient,private userService: UserService) {
    this.httpClient = httpClient
  }

  public addCourseInstance(courseInstanceCreateDto: CourseInstanceCreateDto):Observable<any>{
    return this.httpClient.post(this.courseInstanceUrl + "/post",courseInstanceCreateDto,this.userService.optionWithAuthorizationHeader)
  }

  public getAll():Observable<any>{
    return this.httpClient.get(this.courseInstanceUrl + "/all",this.userService.optionWithAuthorizationHeader) as Observable<any>
  }
}
