import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserService} from "./user.service";
import {CreateAplication} from "../model/aplication/CreateAplication";
import {Observable} from "rxjs";
import {ReturnAplication} from "../model/aplication/ReturnAplication";

@Injectable({
  providedIn: 'root'
})
export class AplicationServiceService {

  private aplicationUrl = '/api/v1/aplication'
   returnAplication: ReturnAplication

  constructor(private httpClient: HttpClient, private userService: UserService) {
  }

  getUserId(): number {
    return this.userService.getUser().id;
  }

  public create(aplicationCreate: CreateAplication): Observable<any> {
    return this.httpClient.post(this.aplicationUrl + '/post', aplicationCreate, this.userService.optionWithAuthorizationHeader) as Observable<any>;
  }

  public returnApl(userId: number): Observable<ReturnAplication> {
    // @ts-ignore
    let ret = this.httpClient.get(this.aplicationUrl + '/getApplication', userId, this.userService.optionWithAuthorizationHeader) as Observable<any>;

    ret.subscribe(r =>{
      this.returnAplication=r
    })
    return ret;
  }
}
