import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserCreateDto} from "../model/user/userCreateDto";
import {BehaviorSubject, Observable} from "rxjs";
import {UserReturnDto} from "../model/user/userReturnDto";
import {UserLoginDto} from "../model/user/userLoginDto";
import firebase from "firebase/compat";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUrl = "/api/v1/user";
  private userReturnDto: UserReturnDto;
  optionWithAuthorizationHeader = {};


  constructor(private httpClient: HttpClient) {
    // // @ts-ignore
    // this.userReturnDto = new UserReturnDto();
  }

  public createUser(user: UserCreateDto): Observable<any> {
    return this.httpClient.post(this.userUrl + "/createuser", user);
  }

  public login(userLoginDto: UserLoginDto): Observable<UserReturnDto> {
    this.optionWithAuthorizationHeader = {
      headers: {
        Authorization: 'Basic ' + btoa(userLoginDto.username + ':' + userLoginDto.password)
      }
    }

    let userReturn = this.httpClient.post(this.userUrl + '/login',
      userLoginDto, this.optionWithAuthorizationHeader) as Observable<UserReturnDto>;
    userReturn.subscribe(x => {
        this.userReturnDto = x
      }
    )
    return userReturn;
  }

  public logout() {
    this.userReturnDto = {
      id: 0,
      userName: '',
      type: '',
      lastName: '',
      firstName: ''
    }
  }

  hasRole(): string {
    return this.getUser().type
  }

  public getUser(): UserReturnDto {
    return <UserReturnDto>this.userReturnDto;
  }

  setUser(user: UserReturnDto) {
    this.userReturnDto = user;
  }


  isAuthenticated(): boolean {
    if (this.userReturnDto != null) {
      return true;
    }
    return false;
  }
}
