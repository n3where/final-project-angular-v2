import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ModuleCreateDto} from "../model/module/moduleCreateDto";
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  private moduleUrl = '/api/v1/module';
  private httpClient;


  constructor(httpClient: HttpClient, private userService: UserService) {
    this.httpClient = httpClient;
  }

  public getAll(): Observable<any> {
    return this.httpClient.get(this.moduleUrl + '/getAll',this.userService.optionWithAuthorizationHeader) as Observable<ModuleCreateDto[]>;
  }

  public addModule(createDto: ModuleCreateDto): Observable<any> {
    return this.httpClient.post(this.moduleUrl + "/post", createDto,this.userService.optionWithAuthorizationHeader);
  }
}
