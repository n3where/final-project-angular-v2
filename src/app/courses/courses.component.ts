import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {ModuleService} from "../service/module.service";
import {ModuleReturnDTO} from "../model/module/moduleReturnDTO";
import {CourseService} from "../service/course.service";
import {Router} from "@angular/router";
import {CourseCreateDto} from "../model/course/courseCreateDto";


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  moduleList: ModuleReturnDTO[] = [];
  courseForm!: FormGroup;
  moduleIds = []


  constructor(private router: Router, private courseService: CourseService, private moduleService: ModuleService, private formBuilder: FormBuilder) {
  }

  changeState(event: any, moduleReturnDTO: ModuleReturnDTO) {
    moduleReturnDTO.isChecked = event.checked
  }

  createCourse() {
    return this.formBuilder.group({
      name: new FormControl(),
      listOfModulesId: new FormControl(),
      description: new FormControl()
    })
  }

  getModulesId(): number[] {
    for (let mod of this.moduleList) {
      if (mod.isChecked) {
        // @ts-ignore
        this.moduleIds.push(mod.id)
      }
    }
    return this.moduleIds
  }

  ngOnInit(): void {
    this.getData();
    this.courseForm = this.formBuilder.group({
      name: new FormControl(''),
      listOfModulesId: new FormControl([]),
      description: new FormControl('')
    })
  }

  getData() {
    this.moduleService.getAll().subscribe((modules) => {
      this.moduleList = modules;
    })
  }

  onSubmit() {


    const cours: CourseCreateDto = {
      name: this.courseForm.get('name')?.value,
      moduleId: this.getModulesId(),
      description: this.courseForm.get('description')?.value
    }
    console.log(cours)

    this.courseService.addCourse(cours).subscribe(response => {
      this.router.navigate([''])
    })

    this.moduleIds = []

  }

}
