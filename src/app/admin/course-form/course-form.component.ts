import { Component, OnInit } from '@angular/core';
import {CourseInstanceReturnDto} from "../../model/courseInstance/CourseInstanceReturnDto";
import {CourseInstanceService} from "../../service/course-instance.service";

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.css']
})
export class CourseFormComponent implements OnInit {

  courseInstanceList: CourseInstanceReturnDto[]

  constructor(private courseInstance: CourseInstanceService) { }

  ngOnInit(): void {
    this.getCourseInstance()
  }

  getCourseInstance(){
    this.courseInstance.getAll().subscribe((courseInstance)=>{
      this.courseInstanceList=courseInstance
    })
  }

}
