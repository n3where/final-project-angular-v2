import {Component, OnInit} from '@angular/core';
import {CourseReturnDto} from "../../model/course/courseReturnDto";
import {CourseService} from "../../service/course.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {ModuleReturnDTO} from "../../model/module/moduleReturnDTO";
import {CourseInstanceCreateDto} from "../../model/courseInstance/CourseInstanceCreateDto";
import {CourseInstanceService} from "../../service/course-instance.service";
import {Router} from "@angular/router";

@Component({
  selector: 'admin-courses',
  templateUrl: './admin-courses.component.html',
  styleUrls: ['./admin-courses.component.css']
})
export class AdminCoursesComponent implements OnInit {

  courseList: CourseReturnDto[] = []
  courseInstanceForm!: FormGroup

  constructor( private router:Router, private courseInstanceService: CourseInstanceService, private courseService: CourseService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.getCourse()
    this.courseInstanceForm = this.formBuilder.group({
      courseId: new FormControl(),
      courseStartDate: new FormControl(),
      courseEndDate: new FormControl(),
      name: new FormControl()
    })
  }

  createInstanceCourse() {
    return this.formBuilder.group({
      courseId: this.getCourseId(),
      courseStartDate: new FormControl(),
      courseEndDate: new FormControl(),
      name: new FormControl()
    })
  }

  // @ts-ignore
  getCourseId(): number {
    for (let cou of this.courseList) {
      if (cou.isChecked) {
        return cou.id
      }
    }
  }

  changeState(event: any, courseReturnDTO: CourseReturnDto) {
    courseReturnDTO.isChecked = event.checked
  }

  getCourse() {
    this.courseService.getAll().subscribe((course) => {
      this.courseList = course
    })
  }

  onSubmit() {
  const courseInstace: CourseInstanceCreateDto ={
    courseId: this.getCourseId(),
    courseStartDate: this.courseInstanceForm.get('courseStartDate')?.value,
    courseEndDate: this.courseInstanceForm.get('courseEndDate')?.value,
    name: this.courseInstanceForm.get('name')?.value
  }

    console.log(courseInstace)

    this.courseInstanceService.addCourseInstance(courseInstace).subscribe(response=>{
      this.router.navigate([''])
    })

  }
}
