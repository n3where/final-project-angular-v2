import { Component, OnInit } from '@angular/core';
import {CourseReturnDto} from "../model/course/courseReturnDto";
import {CourseService} from "../service/course.service";

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  courseList: CourseReturnDto[] = [];

  constructor(private courseService: CourseService) { }

  ngOnInit(): void {
    this.getCourseList()
  }

  getCourseList(){
    this.courseService.getAll().subscribe(response =>{
      this.courseList = response
    })
  }

}
