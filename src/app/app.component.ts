import {Component} from '@angular/core';
import {UserService} from "./service/user.service";
import firebase from "firebase/compat";
import User = firebase.User;
import {UserReturnDto} from "./model/user/userReturnDto";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularTestProjectV1';


  constructor() {
  }
}
