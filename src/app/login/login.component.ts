import {Component, OnInit} from '@angular/core';
import {UserService} from "../service/user.service";
import {Route, Router} from "@angular/router";
import {UserLoginDto} from "../model/user/userLoginDto";
import {CreateAplication} from "../model/aplication/CreateAplication";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {UserCreateDto} from "../model/user/userCreateDto";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  userName: string = '';
  passwordVal: string = '';


  constructor(private userService: UserService,
              private router: Router) {
    if(this.userService.getUser()){
      this.router.navigate(['app-home-page'])
    }
  }

  ngOnInit(): void {
  }



  login() {
    const userLoginDto: UserLoginDto = {
      username: this.userName,
      password: this.passwordVal
    }

    this.userService.login(userLoginDto).subscribe(response => {
      const submitMessage = this.userService.isAuthenticated()
      alert("Welcome " + submitMessage);
      if(submitMessage){
        this.router.navigate([''])
      }else{
        this.router.navigate(['app-login'])
      }
      console.log(this.userService.getUser())
    })
  }


}
