import { Component, OnInit } from '@angular/core';
import {ModuleReturnDTO} from "../model/module/moduleReturnDTO";
import {ModuleService} from "../service/module.service";

@Component({
  selector: 'module-list',
  templateUrl: './module-list.component.html',
  styleUrls: ['./module-list.component.css']
})
export class ModuleListComponent implements OnInit {

  moduleList: ModuleReturnDTO[] = [];

  constructor(private moduleService: ModuleService) { }

  ngOnInit(): void {
    this.moduleService.getAll().subscribe(response =>{
      this.moduleList = response;
    })
  }

  getModuleList(){
    return this.moduleList;
  }

}
